<?php


session_start();
if(!isset($_SESSION['username'])) {
    echo "Please login";
    header( "refresh:1;url=login.php?page=admin" );
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Approval - AIA Music Run</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap-3.2.0-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/dashboard.css" rel="stylesheet">
    <link href="assets/css/dropzone.css" rel="stylesheet">


    <script src="assets/js/dropzone.js"></script>
    <script src="assets/js/jquery-1.11.1.min.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>

        function showLoading(){

            $('#fetchBtn').hide();
            $('#saveBtn').hide();
            $('#uploadBtn').hide();
            $('#exportBtn').hide();
            $('#waitMsg').show();
        };
        var chbData = {};
        $( document ).ready(function() {


            $('#saveBtn').click(function(e){
                var data = {};
//                $('input[data-id]').each(function(e){
//                    var id = $(this).attr('data-id');
//                    var value =  $(this).prop('checked') ? 1 : 0 ;
//                    console.log(id + " > " + value);
//
//                    data[id] = value;
//
//                });

                var pData = {
                    'approve':chbData
                };

                $.post('data.php', pData , function(response){
                    console.log(response);

                  location.reload();
                });
//                $('#target').submit(pData , function(response){
//                    console.log(response);
//                });

                showLoading();
            });

            $('.chb-approve').click(function(e){
                var value =  $(this).children('input[data-id]').prop('checked')  ? 1 : 0;
                var id = $(this).children('input[data-id]').attr('data-id');
                $(this).addClass('pending');
                //$(this).html('pending');

                chbData[id] = value;
                console.log(id + " > " + value);
            });


            $('#fetchBtn').click(function(){
                showLoading();
                $.post('fetch.php' , function(response){
                    location.reload();
                });
            });

            $('#doneUploadBtn').click(function(){
              $('#uploadPopup').modal('hide');
                location.reload();
            });
        });



    </script>

    <style>
        body {
            background-color: linen;
        }

        .thumbnail{
            /*display: -webkit-flex; *//* Safari */
            /*-webkit-align-items: center; *//* Safari 7.0+ */
            /*display: flex;*/
            /*align-items: center;*/
            min-height: 400px;
        }

        .thumbnail .frame{
            width: 100%;
            height: 170px;
        }

        .thumbnail .frame .display{
            float: left;
            width: 150px;
            height: 150px;
            background-size: cover !important;
            background-position: center center !important;
        }

        .thumbnail .frame .detail{
            float: left;
            position: absolute;
            left: 180px;
        }



        .thumbnail .detail>label{
            padding: 10px 10px 10px 30px;
            border-radius: 6px;;
        }

        .thumbnail .caption{
            word-wrap: break-word;
        }

        .chb-approve{
            border: 1px solid grey;
        }

        .chb-approve.approved{
            background-color: rgba(100, 200, 100, 0.5);
            border: 1px solid green;
        }

        .chb-approve.pending{
            background-color: rgba(220, 220, 60, 0.5);
            border: 1px solid green;
        }

        #uploadPopup form{
            min-height: 500px;
        }


        .thumbnail>img{

        }
    </style>
</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">AIA Sharing a Life</a>
        </div>
        <div class="navbar-collapse collapse">
            <form class="navbar-form navbar-right">
                <!--                <input type="text" class="form-control" placeholder="Search...">-->

                <button id="waitMsg" type="button" class="btn btn-warning" style="display: none" > please wait... </button> </a>

                <button id="fetchBtn" type="button" class="btn btn-success" > Fetch </button>
                <button id="uploadBtn" type="button" class="btn btn-success" data-toggle="modal" data-target="#uploadPopup" > Upload </button>
                <a href="export_file.php"> <button id="exportBtn" type="button" class="btn btn-default" > Export </button> </a>
                <a href="#"> <button id="saveBtn" type="button" class="btn btn-primary" > Submit </button> </a>
            </form>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <?php

        include_once('record.php');
        $r = new record();
        $list = $r->getApproveList();


        foreach( $list as $item){

            $check = empty($item->active) ? '' : 'checked';

            $class = empty($item->active) ? 'none' : 'approved';

            $path = $item->source == 'in' ? 'images/thumb/'.$item->external_id.'.jpg' : 'images/local/'.$item->external_id.'.jpg';

            $str_time = date("H:i-j M" , $item->create_time);
            $t = '';
            $t .= '<div class="col-xs-6 col-md-3">';
            $t .= '<div class="thumbnail" data-toggle="modal" data-target="#galleryPopup">';
            $t .= '<div class="frame">';
            $t .= '  <div class="display" width="150" height="150" style="background:url('.$path.')" alt="..."></div>';
            $t .= '  <div class="detail" style="padding:5px">';
            $t .= '    <label class="chb-approve checkbox-inline '.$class.'"> <input type="checkbox" id="chb-'.$item->id.'" data-id="' .$item->id .' "value="" '.$check.' > Approve </label>';
            $t .= '    <p><a href="'.$item->source_link.'">'. $item->username .'</a></p>';
            $t .= '    <p class="like">♥ '. $item->likes .'</p>';
            $t .= '    <p class="time">'. $str_time .'</p>';
            $t .= '  </div>';
            $t .= '</div>';
            $t .= '<div class="caption">' . $item->caption . "</div>";
            $t .= '</div></div>';


            echo $t;
        }


        ?>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="uploadPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
<!--              <button style="padding: 10px" type="button" class="close" data-dismiss="modal"><p aria-hidden="true">X</br></p><span class="sr-only">Close</span></button>-->
              <button id="doneUploadBtn" type="button" class="btn btn-success" > Done </button>
                <form action="data_upload.php"
                      class="dropzone"
                      id="my-awesome-dropzone">

                      </form>


            </div>
        </div>
    </div>
</div>



<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="assets/js/jquery-1.11.1.min.js"></script>
<script src="assets/bootstrap-3.2.0-dist/js/bootstrap.js"></script>
</body>
</html>


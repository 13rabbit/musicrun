<?php

class record
{
    private $db;

    public static $tag = 'TMRbyAIA';
    public static $tag2 = 'MusicRunners';


    private function initDB()
    {
        include_once("Database.php");

        $devMode = strpos($_SERVER['SERVER_NAME'] , "192.") > -1 || strpos($_SERVER['SERVER_NAME'] , "localhost") === 0;

        if( $this->db )return $this->db;

        if ($devMode) {
            $db = new Database();
            //$db = new Database('mysql:host=192.168.1.35;port=3306;dbname=test;charset=utf8;', 'admin', 'prgnarak00171');
            // $db = new Database('mysql:host=localhost;port=3306;dbname=test;charset=utf8;', 'root', '');
        }elseif(strpos($_SERVER['SERVER_NAME'] , 'imcthai') > -1){
            $db = new Database('mysql:host=localhost;port=3306;dbname=test;charset=utf8;', 'apps', 'rI6ftFDlkrKdbGnb');
        } else {
            $db = new Database('mysql:host=localhost;port=3306;dbname=aia_sharingalife;charset=utf8;', 'aia_sharingalife', 'BGXh1Xgt52');
        }
        return $db;
    }


    public function insert( $tableName , $insert)
    {
        $db = $this->initDB();
        $result = $db->insert($tableName , $insert);

        return $result;
    }

    public function update( $tableName , $update , $where)
    {
        $db = $this->initDB();
        $result = $db->update($tableName , $update , $where);

        return $result;
    }

    public function getList()
    {
        $db = $this->initDB();
        $result = $db->query("SELECT * FROM register where status = 1 order by id desc");

        return $result;
    }

    public function getFullList()
    {

        $list = $this->getList();
        $rawData = self::getLocationList();


        foreach( $list as $item ){
            $loc = $item->location_id;
            $loc = explode("-" , $loc);
            $loc_name = $rawData[$loc[0]][0];
            $loc_place = $rawData[$loc[0]][3][$loc[1]]['place'];

            $str_time = date("H:i:s-j M y " , $item->time);
            //print_r($rawData[$loc[0]][3]);

            $types = array('ทั่วไป' , 'ลูกค้า' , 'ตัวแทน');

            $p = '';
            $p .= '<tr>';
            $p .= '<td>' . $item->name . '</td>';
            $p .= '<td>' . $types[$item->type] . '</td>';
            $p .= '<td>' . $item->code . '</td>';
            $p .= '<td>' . $item->tel . '</td>';
            $p .= '<td>' . $item->email . '</td>';
            $p .= '<td>' . $loc_name . '</td>';
            $p .= '<td>' . $loc_place . '</td>';
            $p .= '<td>' . $str_time . '</td>';

            //echo $item->name . ' ' . $item->tel . ' ' .$loc_name . ' -> ' . $loc_place;
            //echo '</br>';
            echo $p;
        }
    }

    public function getApproveList()
    {
        $db = $this->initDB();
//        $result = $db->query("SELECT * FROM photo where tag = '".self::$tag."' AND status = 1 order by id desc");
        $result = $db->query("SELECT * FROM run_photo
            where (tag = '".self::$tag."' OR tag = '".self::$tag2."')
            AND status = 1 order by create_time desc");
        //$result2 = $db->query("SELECT * FROM photo where tag = '".self::$tag2."' AND status = 1 order by create_time desc");



        return $result;

    }


    public function getApprovedList($offset = 0 , $limit = 20)
    {
        $db = $this->initDB();
        $sql = "SELECT * FROM run_photo
        where (tag = '".self::$tag."' OR tag = '".self::$tag2."')
        AND active = 1 AND status = 1
        order by flag, create_time desc limit ".$offset.",".$limit;



        $result = $db->query($sql);

        return $result;
    }

    public function getApprovedListRand($offset = 0, $limit = 40 , $slot = 12)
    {
        // max slot = 12
        $db = $this->initDB();

        //for vip
        $sql = "SELECT * FROM run_photo
          where (tag = '".self::$tag."' OR tag = '".self::$tag2."')
          AND flag = 2
          AND active = 1 AND status = 1
          order by create_time limit ".$slot;

        $resultvip = $db->query($sql);

        $sql = "SELECT * FROM run_photo
          where (tag = '".self::$tag."' OR tag = '".self::$tag2."')
          AND flag = 0
          AND active = 1 AND status = 1
          order by create_time limit ".$slot;

        $result = $db->query($sql);


        // add random 40
        $sql = "SELECT * FROM run_photo
        where (tag = '".self::$tag."' OR tag = '".self::$tag2."')
        AND flag = 1
        AND active = 1 AND status = 1
        order by RAND() limit ".$limit;


        $result2 = $db->query($sql);
        $result = array_merge($resultvip , $result , $result2);



        return $result;
    }

    public static function getLocationList()
    {
        // ------------------------------------------- //
        $rawData = array();

        $rawData[] = array(
            'กรุงเทพ' , 170, 348 , array(
                array('place'=>'ศูนย์กีฬาเยาวชนสะพานพระราม 9' , 'act'=>'ปรับปรุงลานกีฬา '  , 'time'=>'8.00-14.00น.', 'lat'=>13.677739 , 'long'=> 100.513761),
                array('place'=>'ถนนนราธิวาส แยก ITF ', 'act'=>'ปรับปรุงลานกีฬา ตัดแต่งต้นไม้' , 'time'=>'8.45-12.50น.' , 'lat'=>13.727792 , 'long'=>100.527493),
                array('place'=>'สถานสงเคราะห์เด็กอ่อน รังสิต', 'act'=>'ปรับปรุงสภาพภูมิทัศน์ ', 'time'=>'7.00-14.00น.' , 'lat'=>14.011402,  'long'=> 100.713065),
                array('place'=>'ลานกีฬากรุงเทพแข็งแรง บางโคล่', 'act'=>'ปรับปรุงลานกีฬาและตรวจสุขภาพชุมชน', 'time'=>'7.30-14.30น.' , 'lat'=>13.697453 , 'long'=>100.521299),
                array('place'=>'สวนรมณีนาถ ', 'act'=>'ปรับปรุงลานกีฬาและตรวจสุขภาพชุมชน ', 'time'=>'7.00-13.30น.' , 'lat'=>13.749211 , 'long'=>100.502578),
                array('place'=>'สวนสันติชัยปราการ', 'act'=>'ปรับปรุงลานกีฬาและตรวจสุขภาพชุมชน ', 'time'=>'7.00-13.30น.' , 'lat'=>13.764014,   'long'=>100.495728),
                array('place'=>'สวนลุมพินี ', 'act'=>'ปรับสภาพภูมิทัศน์และตรวจสุขภาพชุมชน ', 'time'=>'11.00-17.00น.' , 'lat'=>13.732896 , 'long'=>100.544073)
            )
        );

        $rawData[] = array('เชียงราย ' , 146,66 , array( array('place'=>'โรงเรียนพานพิทยาคม ', 'act'=>'ซ่อมแซมหลังคา ทาสีกำแพงโรงเรียน', 'time'=>'8.30-13.30 น.' , 'lat'=>19.534509 , 'long'=>99.742153) ));
        $rawData[] = array('เชียงใหม่' , 80, 98 , array( array('place'=>'สวนบวกหาด  ' , 'act'=>'ปรับปรุงสวนสาธารณะ' , 'time'=>'8.30-13.00 น.', 'lat'=>18.781788 , 'long'=>98.979432)));
        $rawData[] = array('นครสวรรค์' , 185, 243 , array( array('place'=>'อุทยานสวรรค์  ' , 'act'=>'ปรับปรุงลู่วิ่งจักรยาน ปรับปรุงเครื่องออกกำลังกาย' , 'time' => '15.00-17.45 น.' , 'lat'=>15.697689 , 'long'=>100.122168)));
        $rawData[] = array('ตาก' , 96, 167 , array( array('place'=>'ริมปิง' , 'act'=>'ปรับปรุงสวนสาธารณะ' , 'time' => '8.30-13.30 น.' , 'lat'=>16.884573  , 'long'=>99.117314)));
        $rawData[] = array('อุดรธานี' , 274, 186 , array( array('place'=>'สวนสาธารณะหนองสิม  ' , 'act'=>'ปรับปรุงลานกีฬาและสวนสาธารณะ' , 'time' => '9.00-13.00 น.' , 'lat'=>17.419917 , 'long'=>102.801626)));
        $rawData[] = array('ขอนแก่น' , 321, 213 , array( array('place'=>'สวนสาธารณะบึงหนองโคตร  ' , 'act'=>'ปรับปรุงลานกีฬาและสวนสาธารณะ' , 'time' => '13.00-18.00 น.' , 'lat'=>16.435308 , 'long'=>102.801935)));
        $rawData[] = array('นครราชสีมา  ' , 318, 286 , array( array('place'=>'โรงเรียนสระแก้ว ' , 'act'=>'ปรับปรุงเครื่องออกกำลังกาย' , 'time' => '9.00-14.00 น.' , 'lat'=>14.971479 , 'long'=>102.101783)));
        $rawData[] = array('อุบลราชธานี' , 395, 285 , array( array('place'=>'ลานกีฬากลางแจ้งหมู่บ้านลับแล อ.วารินชำราบ' , 'act'=>'ปรับปรุงเครื่องออกกำลังกาย' , 'time' => '8.00-13.00 น.' , 'lat'=>15.200338 , 'long'=>104.858963)));
        $rawData[] = array('สุพรรณบุรี' , 125 , 261 , array( array('place'=>'โรงเรียนวัดคันทด ' , 'act'=>'ปรับปรุงสนามเด็กเล่น ปลูกต้นไม้' , 'time' => '8.30-12.30 น.' , 'lat'=>14.449219 , 'long'=>100.150464)));
        $rawData[] = array('ราชบุรี' , 118, 352 , array( array('place'=>' โรงเรียนวันครู' , 'act'=>'ปรับปรุงสภาพภูมิทัศน์' , 'time' => '8.30-12.45 น.' , 'lat'=>13.627529 , 'long'=>99.576731)));
        $rawData[] = array('ระยอง' , 220, 383 , array( array('place'=>'โรงเรียนวัดหนองสนม' , 'act'=>'ปรับปรุงลานกีฬา' , 'time' => '8.30-12.30 น.' , 'lat'=>12.687991 , 'long'=>101.201324)));
        $rawData[] = array('นครศรีธรรมราช ' , 160, 640 , array( array('place'=>'สวนสาธารณะเฉลิมพระเกียรติ ' , 'act'=>'ปรับปรุงลานกีฬา' , 'time' => '15.00-18.30 น.' , 'lat'=>8.416098 , 'long'=>99.961532)));
        $rawData[] = array('ตรัง' , 156, 689 , array( array('place'=>'สวนพระยารัษฎานุประดิษฐ์  ' , 'act'=>'ปรับปรุงสวนสาธารณะ' , 'time' => '8.00-13.00 น.' , 'lat'=>7.565217 , 'long'=>99.623127)));
        $rawData[] = array('สงขลา' , 138, 589 , array( array('place'=>'วัดโคกนาว ' , 'act'=>'ปรับสภาพภูมิทัศน์' , 'time' => '8.30-13.00 น.' , 'lat'=>7.005839 , 'long'=>100.492013)));

        return $rawData;

        // ------------------------------------------- //
    }


}
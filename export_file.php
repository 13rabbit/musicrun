<?php

session_start();
if(!isset($_SESSION['username'])) {
    echo "Please login";
    header("Location: login.php");
    exit();
}

//$date = date('Ymd');
//$file_name = "file_" . $date . ".csv";
//header("Content-type: text/csv; charset=utf-8");
//header("Content-Disposition: attachment; filename=" . $file_name);
//header("Pragma: no-cache");
//header("Expires: 0");

//header('Content-type: text/csv; charset=utf-8');
//header("Content-Disposition: attachment; filename=" . $file_name);

$zipname = 'export.zip';
$zip = new ZipArchive;
$zip->open($zipname, ZipArchive::CREATE);
$dir = "images/img/";
if ($handle = opendir($dir)) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != ".." && !strstr($entry,'.php')) {
            $zip->addFile($dir. $entry  , $entry);
        }
    }
    closedir($handle);
}

$zip->close();

//exit();
ob_get_clean();
header('Content-Type: application/zip');
header("Content-Disposition: attachment; filename=".$zipname);
header("Content-Transfer-Encoding: binary");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header('Content-Length: ' . filesize($zipname));
header("Pragma: no-cache");
header("Expires: 0");
readfile($zipname);
//header("Location: adcs.zip");




CREATE TABLE IF NOT EXISTS `run_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(2) NOT NULL,
  `external_id` varchar(128) NOT NULL,
  `tag` varchar(128) NOT NULL,
  `source_link` varchar(256) NOT NULL,
  `username` varchar(128) NOT NULL,
  `user_fullname` varchar(256) NOT NULL,
  `user_pic` varchar(256) NOT NULL,
  `caption` varchar(256) NOT NULL,
  `likes` int(11) NOT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `store_time` int(11) unsigned NOT NULL,
  `flag` int(4) NOT NULL DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=187 ;




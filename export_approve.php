<?php

session_start();
if(!isset($_SESSION['username'])) {
    echo "Please login";
    header("Location: login.php");
    exit();
}

$date = date('Ymd');
$file_name = "aiasal_" . $date . ".csv";
//header("Content-type: text/csv; charset=utf-8");
//header("Content-Disposition: attachment; filename=" . $file_name);
//header("Pragma: no-cache");
//header("Expires: 0");

header('Content-type: text/csv; charset=utf-8');
header("Content-Disposition: attachment; filename=" . $file_name);

include_once("record.php");
$rec = new record();
//$list = $rec->getList();
$list = $rec->getApproveList();


$header = ['"Tag"' , '"Username"' , '"Fullname"' , '"Caption"' , '"Like"' , '"Time"' ,'"Approved"' , '"Link"', 'Photo'];

$r = "";
$r .= join("," , $header);
$r .= "\n";

foreach( $list as $item ){
    $row = [];

    $img_url = 'http://campaigns.aia.co.th/sharingalife/images/img/'.$item->external_id.'.jpg';
    $str_time = date("H:i:s - j M y" , $item->create_time);

    $row[] = '"'. $item->tag .'"';
    $row[] = '"'. $item->username .'"';
    $row[] = '"'. $item->user_fullname  .'"';
    $row[] = '"'. $item->caption .'"';
    $row[] = '"'. $item->likes .'"';
    $row[] = '"'. $str_time .'"';
    $row[] = '"'. ($item->active ? 'Approved' : '') .'"';
    $row[] = '"'. $item->source_link . '"';
    $row[] = '"'. $img_url . '"';

    $r .= join("," , $row);
    $r .= "\n";

}

mb_convert_encoding($r, 'UTF-16LE', 'UTF-8');
echo "\xEF\xBB\xBF";
echo $r;
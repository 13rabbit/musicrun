<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 28/9/2557
 * Time: 12:48 น.
 */

include_once('record.php');



$client_id = '62b2f6e78fcf4036a071980ce7c1bc94';
if(isset($_GET['tag'])){
    $tag = $_GET['tag'];
} else {
    $tag = record::$tag;
}

if(isset($_GET['fetch_limit'])){
    $fetch_limit = $_GET['fetch_limit'];
} else {
    $fetch_limit = 0;
}



$folder = "images/";
$url_affix = '';
$fetch_count = 0;


if(isset($_GET['next_max_id'])){
    $url_affix = '&max_tag_id=' .$_GET['next_max_id'];
    $fetch_count= intval($_GET['fetch_count']) + 1;
}


set_time_limit(600);

$url = "https://api.instagram.com/v1/tags/{$tag}/media/recent?client_id=".$client_id.$url_affix;


$result = json_decode(file_get_contents($url));




echo $url;
echo "</br>";
echo "Fetch count: " . $fetch_count;
echo "</br>";
echo "Total file receive: " . count($result->data);
echo "</br>";

$file_exist_count = 0;

foreach( $result->data as $item){


    if( $item->type != 'image') continue;
    //var_dump($item->images->standard_resolution->url);

    //if( $item->type != 'images') continue;
    $id = $item->id;
    $img = $item->images->standard_resolution;
    $img_url = $img->url;
    $img_thumb = $item->images->thumbnail->url;
    $width = $img->width;
    $height = $img->height;

    $like = $item->likes->count;
    $link = $item->link;

    $img_file = $folder ."img/". $id . ".jpg";
    $img_thumb_file = $folder ."thumb/". $id . ".jpg";


    if( file_exists($img_file) ) {
        $file_exist_count++;
        echo "</br>";
        echo "file exist! : " .$img_file;
        echo "</br>";

        // update like
        $r = new record();
        $r->update('run_photo' , array('likes' => $like) , array('external_id' => $id));


        // check file size

        if(filesize($img_file) < 1000){
            file_put_contents($img_file , file_get_contents($img_url));

        }

        if(filesize($img_thumb_file) < 1000){
            file_put_contents($img_thumb_file , file_get_contents($img_thumb));
        }

        continue;
    }

    //echo "image link > " . $img_url;

    file_put_contents($img_file , file_get_contents($img_url));

    file_put_contents($img_thumb_file , file_get_contents($img_thumb));


    //var_dump($item->user); exit();

    echo "<img src='". $img_file ."'/>";

    $record = array(
        'source' => 'in',
        'external_id' => $id,
        'tag'   => $tag,
        'source_link' => $link,
        'username' => $item->user->username,
        'user_fullname' => $item->user->full_name,
        'user_pic'  => $item->user->profile_picture,
        'caption' => str_replace("'", "\\'" ,$item->caption->text ),
        'likes' => $like,
        'create_time' => $item->created_time,
        'store_time' => time(),
    );

    $r = new record();
    $r->insert('run_photo', $record);

    //var_dump($record);
}


// fetch next page
if( $fetch_count < $fetch_limit){
    //header("Location:fetch.php?next_max_id=" . $result->pagination->next_max_id."&fetch_count=".$fetch_count);
    $next_url = "fetch.php?next_max_id=" . $result->pagination->next_max_id."&fetch_count=".$fetch_count."&fetch_limit=".$fetch_limit;
    header("refresh:2;url=".$next_url);
}
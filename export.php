<?php

session_start();
if(!isset($_SESSION['username'])) {
    echo "Please login";
    header("Location: login.php");
    exit();
}

$date = date('Ymd');
$file_name = "reg_" . $date . ".csv";
//header("Content-type: text/csv; charset=utf-8");
//header("Content-Disposition: attachment; filename=" . $file_name);
//header("Pragma: no-cache");
//header("Expires: 0");

header('Content-type: text/csv; charset=utf-8');
header("Content-Disposition: attachment; filename=" . $file_name);

include_once("record.php");
$rec = new record();
$list = $rec->getList();
$rawData = record::getLocationList();


$header = ['"Name"' , '"Type"', '"Identification"' , '"Tel"' , '"email"' , '"province"' , '"area"' ,'"time"'];

$r = "";
$r .= join("," , $header);
$r .= "\n";

$types = array('ทั่วไป' , 'ลูกค้า' , 'ตัวแทน');

foreach( $list as $item ){
    $row = [];
    $loc = $item->location_id;
    $loc = explode("-" , $loc);
    $loc_name = $rawData[$loc[0]][0];
    $loc_place = $rawData[$loc[0]][3][$loc[1]]['place'];


    $str_time = date("H:i:s-j M y " , $item->time);

    $row[] = '"'. $item->name .'"';
    $row[] = '"'. $types[$item->type] . '"';
    $row[] = '"'. $item->code .'"';
    $row[] = '"'. $item->tel  .'"';
    $row[] = '"'. $item->email .'"';
    $row[] = '"'. $loc_name .'"';
    $row[] = '"'. $loc_place .'"';
    $row[] = '"'. $str_time .'"';

    $r .= join("," , $row);
    $r .= "\n";

}

mb_convert_encoding($r, 'UTF-16LE', 'UTF-8');
echo "\xEF\xBB\xBF";
echo $r;
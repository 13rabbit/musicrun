<?php
include_once('record.php');

$offset = (@$_GET['o']) ? @$_GET['o'] : 0;
$limit = (@$_GET['l']) ? @$_GET['l'] : 50;


$hostname = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];

$record = new record();

$list =$record->getApprovedListRand($offset, $limit);
if (count($list) <= 0) {
  $offset = 0;
  $list =$record->getApprovedListRand($offset, $limit);
}

function getImgPath($item){
  //$hostname = 'http://'.$_SERVER['HTTP_HOST'].'/musicrun/';
  $ext = $item->external_id;

  if( $item->source == 'in'){
      $img = 'images/img/'.$ext.'.jpg';
  }
  else {
      $img = 'images/local/'.$ext.'.jpg';
  }
  return $img;
}

$frontImg = '';
$bgImg = '';
$bgSpa = '';
$count = 1;
//foreach($list as $item) {
for( $i = 0 ; $i < 15 ; $i++){
  $item = $list[($i % count($list))];
  $ext = $item->external_id;
  $img = getImgPath($item);
  if ($count <= 14) {
    $class = '';
    switch ($count) {
      case 1:
      case 2:
      case 12:
        $class = 'front-img-right';
        break;
      case 3:
      case 4:
        $class = 'front-img-right-middle';
        break;
      case 5:
      case 6:
      case 7:
        $class = 'front-img-right-small';
        break;
      case 8:
      case 9:
        $class = 'front-img-right-mini-top';
        break;
      case 10:
      case 11:
        $class = 'front-img-right-mini-bottom';
        break;
      case 13:
        $frontImg .= '<div id="front-img-'.$count.'" class="front-img-text">
          <p>RUN TO THE <span>BEAT</span></p>
        </div>';
        break;
      case 14:
        $frontImg .= '<div id="front-img-'.$count.'" class="front-img-text">
          <p class="tmr">#TMRbyAIA</p>
        </div>';
        break;
      case 15:
        break;
      case 16:
        break;

      // case 1:
      // case 2:
      //   $class = 'front-img-right';
      // break;
      // case 3:
      //   $class = 'front-img-b-right';
      // break;
      // case 4:
      //   $class = 'front-img-bottom';
      //   $bgSpa .= '<div class="bg-spacial" id="bg-img-'.$count.'" style="background-image: url('.$img.');"></div>';
      // break;
      // case 5:
      //   $class = 'front-img-bottom';
      //   $bgSpa .= '<div class="bg-spacial" id="bg-img-'.$count.'" style="background-image: url('.$img.');"></div>';
      // break;
      // case 6:
      // case 7:
      // case 8:
      //   $class = 'front-img-right-small';
      // break;
      // case 9:
      // case 10:
      //   $class = 'front-img-right-mini-top';
      // break;
      // case 11:
      // case 12:
      //   $class = 'front-img-right-mini-bottom';
      // break;
      // case 13:
      //   $class = 'front-img-right';
      // break;
      // case 14:
        // $frontImg .= '<div id="front-img-'.$count.'" class="front-img-text">
        //   <p>RUN TO THE <span>BEAT</span></p>
        // </div>';
      // break;
      // case 15:
      //   $frontImg .= '<div id="front-img-'.$count.'" class="front-img-text">
      //     <p class="tmr">#TMRbyAIA</p>
      //   </div>';
      // break;
    }

    if ($count <= 12) {
      $frontImg .= '<div id="front-img-'.$count.'" class="front-img '.$class.'" style="background-image: url('. $img . ');"></div>';
    }    

    // Update image front
    $record->update('run_photo' , array('flag' => '1') , array('external_id' => $item->external_id));
  }
  $count++;
}

$total = 41;
for ($i = 0 ; $i < $total ; $i++){
    //$item = $list[rand(0, count($list) - 1)];
    $item = $list[($i % count($list))];
    $ext = $item->external_id;
    $img = getImgPath($item);

    $bgImg .= '<div class="img-box" style="background-image: url('. $img . ');"></div>';
    }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AIA Music Run</title>

    <!-- Bootstrap -->
    <link href="assets/bootstrap-3.2.0-dist/css/bootstrap.min.css" rel="stylesheet">

    <link href="assets/css/slide.css" rel="stylesheet">

    <script type="text/javascript">
      OFFSET = <?php echo $offset?>;
      LIMIT = <?php echo $limit?>;
      HOSTNAME = '<?php echo $hostname?>';
    </script>

  </head>
  <body>
    <div id="slide-stage">
      <div id="all-content">
        <div  id="main-content">
          <div id="backgroup-layer">
            <div class="backgroup-container">
              <?php echo $bgImg;?>
              <div class="black-overlay"></div>
            </div>
          </div>
          <?php echo $bgSpa;?>
          <div id="white-bg" class="white-bg"></div>

        </div>
        <div id="logo-bg" style="background-repeat: no-repeat;  background-position: center center;  background-color: white;"></div>

        <div id="logo-bg-aia" style="background-size: cover;  background-repeat: no-repeat;  background-position: center center;"></div>
        <?php echo $frontImg;?> 
      </div>
    </div>

    <script src="assets/js/jquery-1.11.1.min.js"></script>
    <script src="assets/bootstrap-3.2.0-dist/js/bootstrap.min.js"></script>

    <script src="assets/js/vague.js"></script>
    <script src="assets/js/slide.js"></script>
  </body>
</html>
<?php


class Database extends PDO{

    const PARAM_host='localhost';
    const PARAM_port='3306';
    const PARAM_db_name='test';
    const PARAM_user='root';
    const PARAM_db_pass='';

    public function __construct($a = false , $b = false, $c =false,$options=null){
        if( empty( $a )){
            parent::__construct(
                'mysql:host='.Database::PARAM_host.';port='.Database::PARAM_port.';dbname='.Database::PARAM_db_name.";charset=utf8;",
                Database::PARAM_user,
                Database::PARAM_db_pass,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8") );
        }
        else{
            parent::__construct(
                $a,
                $b,
                $c,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8") );
        }


    }

    public function query($query){ //secured query with prepare and execute
        $args = func_get_args();
        array_shift($args); //first element is not an argument but the query itself, should removed

        $reponse = parent::prepare($query);
        $reponse->execute($args);

        return $reponse->fetchAll(PDO::FETCH_CLASS);


    }

    public function insert( $tableName , $insert)
    {
        $keys = array();
        $values = array();
        foreach( $insert as $key => $value){
            $keys[] = $key;
            $values[] = $value;
        }

        $column = "(" . join("," , $keys) . ")";
        $values_sql = "('" . join("','" , $values) . "')";

        $sql = "Insert into ".$tableName." ".$column." values ".$values_sql;

        parent::exec("set names utf8");
        
        $response = parent::prepare($sql);
        try {
            $status = $response->execute();
            if( !$status ){
                print_r("PDO error: insert error <br/>");
                print_r($sql);
                print_r("<br/>");
            }
        }
        catch (PDOException $e){
            print_r("PDO error:" . $e-getMessage()."<br/>");
            return false;
        }

        return $this->lastInsertId();
    }

    public function update( $tableName , $update , $where)
    {
        $sets = array();
        foreach( $update as $key => $value){

            $sets[] = "".$key."='" .$value ."'";
        }

        $setSql = join( "," , $sets);

        $wheres = array();
        foreach( $where as $key => $value){
            $wheres[] = "".$key."='" .$value ."'";
        }

        $whereSql = join(" AND " , $wheres );

        $sql = "UPDATE " .$tableName . " SET " . $setSql . "  WHERE " . $whereSql ;

        parent::exec("set names utf8");
        
        $response = parent::prepare($sql);

        try {
            $status = $response->execute();
            if( !$status ){
                print_r("PDO error: update error <br/>");
                return false;
            }
        }
        catch (PDOException $e){
            print_r("PDO error:" . $e-getMessage()."<br/>");
            return false;
        }

        return true;
    }


} 
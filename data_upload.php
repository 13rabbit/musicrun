<?php

include_once('record.php');
$ds          = DIRECTORY_SEPARATOR;  //1
$storeFolder = 'images/local/';   //2

if (!empty($_FILES)) {

    $tempFile = $_FILES['file']['tmp_name'];

    $targetPath = dirname( __FILE__ ) . $ds. $storeFolder . $ds;


    if(strpos($_FILES['file']['name'],'.') < 0){
        $raw = $_FILES['file']['name'] . ".jpg";
    } else {
        $raw = $_FILES['file']['name'];
    }

    $raw = str_replace(" ", "_" , $raw);

    $name = explode('.', $raw);
    $name = $name[0] ."_".time();
    $filename = $name . '.jpg';

    $targetFile =  $targetPath. $filename;

    if( !file_exists($targetFile)){


        $record = array(
            'source' => 'lo',
            'external_id' => $name,
            'tag'   => record::$tag,
            'source_link' => '',
            'username' => 'AIA',
            'user_fullname' => 'AIA Music Run',
            'user_pic'  => '',
            'caption' => 'Uploaded photo by AIA',
            'likes' => 0,
            'create_time' => time(),
            'store_time' => time(),
        );

        $r = new record();
        $r->insert('run_photo', $record);


        move_uploaded_file($tempFile,$targetFile);

        echo 'done';
    } else {
        echo 'file exist';
    }



}
else {
    echo 'no file';
}
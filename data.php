<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 31/8/2557
 * Time: 16:29 น.
 */



if( isset( $_POST['name'] )){

    include_once('record.php');
    $rec = new record();
    $time = time();
    $id = $rec->insert('register' , array(
        'name' => $_POST['name'],
        'tel'  => $_POST['tel'],
        'location_id' => $_POST['locationId'],
        'email' => $_POST['email'],
        'code' => $_POST['code'],
        'time' => $time,
        'type' => $_POST['type']
    ));



    echo "register complete : " + $id;

}

else if( isset($_POST['approve'])){

    include_once('record.php');
    $rec = new record();
    $str = '';
    foreach($_POST['approve'] as $id => $value ){
        if(empty($value)){
          $rec->update('run_photo', array('active'=>$value, 'flag'=>0) , array('id'=>$id));
          $str .= $id . ' > ';
        }
        else {
          $rec->update('run_photo', array('active'=>$value) , array('id'=>$id));
        }

    }

    echo "done:". $str;
}

else if( isset($_POST['offset'])){

    include_once('record.php');
    $rec = new record();

    $data = $rec->getApprovedList($_POST['offset']);

    echo json_encode($data);
}

else {
    echo 'data missing';
}

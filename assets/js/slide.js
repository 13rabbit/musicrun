var slidePage = 0;
var imgCount = 0;
var transTime = '0.8';
var timemod = 5 / 2;
var duration = 2 * timemod;
var transitionCode = "transform 0.5s east-out";
var totalPage = 11;

var eleMain = $("#main-content");
var $imgs = $('div.front-img');

$( document ).ready(function() {

  // fetch new image
  setTimeout(function(){
      var url = '';
      url = HOSTNAME + "/fetch.php?fetch_limit=2";
      url = url.replace("/index.php" ,"");
      $.get(url);
  },5000);


  screenW = $('#slide-stage').width();
  screenH =$('#slide-stage').height();
  screenHalfW = (screenW / 2);
  screenHalfH = (screenH / 2);

  l = (screenHalfW - ($(".front-img").width() / 2));
  t = (screenHalfH - ($(".front-img").height() / 2));
  $(".front-img").css('left', l);
  $(".front-img").css('top', t);

  l = (screenHalfW - ($(".front-img-text").width() / 2));
  t = (screenHalfH - ($(".front-img-text").height() / 2));
  $(".front-img-text").css('left', l);
  $(".front-img-text").css('top', t);

  l = (screenHalfW - ($(".front-img-right-small").width() / 2));
  t = (screenHalfH - ($(".front-img-right-small").height() / 2));
  $(".front-img-right-small").css('left', l);
  $(".front-img-right-small").css('top', t);

  l = (screenHalfW - ($(".front-img-right-middle").width() / 2));
  t = (screenHalfH - ($(".front-img-right-middle").height() / 2));
  $(".front-img-right-middle").css('left', l);
  $(".front-img-right-middle").css('top', t);

  l = (screenHalfW - ($(".front-img-right-mini-top").width() / 2));
  t = ( ((screenHalfH / 2) + ((screenHalfH / 8))) - ($(".front-img-right-mini-top").height() / 2));
  $(".front-img-right-mini-top").css('left', l);
  $(".front-img-right-mini-top").css('top', t);

  l = (screenHalfW - ($(".front-img-right-mini-bottom").width() / 2));
  t = ( ((screenHalfH + ((screenHalfH / 8)*3))-10) - ($(".front-img-right-mini-bottom").height() / 2));
  $(".front-img-right-mini-bottom").css('left', l);
  $(".front-img-right-mini-bottom").css('top', t);

  $(".bg-spacial").height(screenH);

//  var vague = $('#main-content').Vague({
//    intensity: 3
//  });
  //vague.blur();

  elementGroup = ".front-img-right, .front-img-right-small, .front-img-right-mini-top, .front-img-right-mini-bottom, .front-img-text, .front-img-right-middle";
  $(elementGroup).css('transform', 'translateX(1000px)');
  $(elementGroup).css('opacity', 1);

  $(".front-img-b-right").css('transform', 'translate(1000px, 1000px)');
  $(".front-img-b-right").css('opacity', 1);

  $(".front-img-bottom").css('transform', 'translateY(1000px)');
  $(".front-img-bottom").css('opacity', 1);

  $(".front-img-left").css('transform', 'translateX(-1000px)');
  $(".front-img-left").css('opacity', 1);


  function playSlide(){
    if (slidePage > totalPage){
      //clearInterval(intervalID);

      url = HOSTNAME + "" + "?o=" + (OFFSET+LIMIT) + "&l=" + LIMIT;
      $(location).attr('href',url);

    } else {
      var s_duration = duration;
      trans(slidePage);
      transAll(slidePage);
      transImg(imgCount);

      if (slidePage == 3) {
        s_duration = duration + 1.5;
        setTimeout(function(){
          transImg(imgCount);
          imgCount++;
        }, 1500);
      }

      if (slidePage == 4) {
        s_duration = duration + 2.0;

        setTimeout(function(){
          transImg(imgCount);
          imgCount++;
        }, 1200);

        setTimeout(function(){
          transImg(imgCount);
          imgCount++;
        }, 2000);
      }

      if (slidePage == 5) {
        s_duration = duration + 1.5;

        setTimeout(function(){
          transImg(imgCount);
          imgCount++;
        }, 500);


        setTimeout(function(){
          transImg(imgCount);
          imgCount++;
        }, 1000);


        setTimeout(function(){
          transImg(imgCount);
          imgCount++;
        }, 1500);


        // setTimeout(function(){
        //   transImg(imgCount);
        //   imgCount++;
        // }, 500);

        // setTimeout(function(){
        //   transImg(imgCount);
        //   imgCount++;
        // }, 1000);
      }

        if( slidePage == 0){
            s_duration = 1;
        }

      if( slidePage == 10){
          s_duration = 1;
      }

      if( window.callStop ) return;
      setTimeout(playSlide, s_duration * 1000);
      imgCount++;
      slidePage++;

    }

  };


  playSlide();
  //$('#bg-img-4').css('transition', 'transform 0.0s ease-out');
  //$('#bg-img-4').css('transform' , 'translate(1024px,0)');
  //
  //$('#bg-img-5').css('transition', 'transform 0.0s ease-out');
  //$('#bg-img-5').css('transform' , 'translate(0px,768px)');
  //
  //$('#white-bg').css('transition', 'transform 0.0s ease-out');
  //$('#white-bg').css('transform' , 'translate(1024px,0');

  $('#all-content').css('transition' , 'transform ' + duration + 's ease-in-out');
});

function transAll(page)
{
  switch (page){
    case 1:
      setAnimation($('#all-content') , ['scale(1.0)'] ,['scale(1.20)' , 'translate(0,30px)' , 'rotate(-8deg)'] , 10 *timemod)
      break;
    case 2:
      setAnimation($('#all-content') , null ,['scale(1.10)' , 'translate(0,30px)' ,'rotate(0deg)'] , 7 *timemod);
      break;
    case 3:
      setAnimation($('#all-content') , null ,['scale(1.20)'  , 'translate(0,70px)','rotate(0deg)'] , 10 *timemod);
      break;
    case 5:
       setAnimation($('#all-content') , null ,['scale(1.15)' , 'translate(0,30px)' ,'rotate(0deg)'] , 10 *timemod );
       break;
    case 6:
      setAnimation($('#all-content') , null ,['scale(1.0)'  , 'translate(0,0px)','rotate(0deg)'] , 8 * timemod)
      break;
  }
}

function trans(page)
{
  console.log("Show page " + page);
  switch (page) {
    case 0:
      $("body").css('background-color', '#D11E48');
      setAnimation($('#logo-bg-aia') , null, ['translate(0,0px)']);
      setAnimation($('#backgroup-layer') , null, ['translate(-1500px,0)']);
    break;
    case 1:
      $("body").css('background-color', '#FFF');
      //$('#backgroup-layer').css('transition', transitionCode);
      //$('#all-content').css("transform", 'scale(1.02)');
      setAnimation($('#logo-bg-aia') , null, ['translate(-1200px,0px)']);
        setAnimation($('#white-bg') , null, ['translate(8px, 0)']);

    break;
    case 2:
      //$('#backgroup-layer').css("transform", 'scale(1.04) translate(-100px , 0px)');
      // setAnimation($('#backgroup-layer') , null, ['translate(-100px , 0px)']);

    break;
    case 3:

      //$('#backgroup-layer').css("transform", 'scale(1.04) translate(0px , -100px)');
      setAnimation($('#white-bg') , null, ['translate(-1200px, 0)']);
      setAnimation($('#backgroup-layer') , ['translate(1200px,0)'], ['translate(0)']);
    break;
     case 4:

       setAnimation($('#backgroup-layer') ,null, ['translate(-50px,0px)']);
    //   setAnimation($('#bg-img-4') ,['translate(0,768px)'], ['translate(0,0)'] , 0.3);
      break;
    case 5:
    //   setAnimation($('#bg-img-4') , null, ['translate(0,-768px)']);
    //   setAnimation($('#bg-img-5') , ['translate(0px,768px)'], ['translate(0,0)'] , 0.3);
      setAnimation($('#backgroup-layer') ,null, ['translate(-100px,0px)']);
      break;
    case 6:
      // setAnimation($('#bg-img-5') , null, ['translate(-1024px,0)']);
      setAnimation($('#backgroup-layer') , null, ['translate(-1200px,0)']);
      setAnimation($('#white-bg') ,  ['translate(1200px,0)'], ['translate(0)']);
    break;
    case 7:
      //setAnimation($('#white-bg') , null, ['translate(-1024px,0)']);
      setAnimation($('#white-bg') , null, ['translate(-1000px, 0)']);
      setAnimation($('#backgroup-layer') , ['translate(1200px, 0px)'], ['translate(0)']);
    break;
    case 8:

      setAnimation($('#backgroup-layer') , null, ['translate(-100px,0)']);
      // setAnimation($('#white-bg') , ['translate(1024px,0)'], ['translate(0px,0)']);

    break;
    // case 9:
    //   setAnimation($('#white-bg') , null, ['translate(-1024px,0)']);
    //   setAnimation($('#backgroup-layer') , ['translate(1024px , 0px)'], ['translate(0,0)']);
    // break;
    // case 10:
    //   //setAnimation($('#white-bg') , null, ['translate(-1024px,0)']);
    //   setAnimation($('#backgroup-layer') , null, ['translate(-100px,0)']);
    // break;
    case 9:
      setAnimation($('#backgroup-layer') ,null , ['translate(-100px,768px)']);
      setAnimation($('#logo-bg') , ['translate(0px , -768px)'], ['translate(0px,0px)' , 'scale(1)']);
      break;
    case 10:
      setAnimation($('#logo-bg') , null, ['translate(0,768px)']);
      $("body").css('background-color', '#D11E48');
      setAnimation($('#logo-bg-aia') , ['translate(0px , -768px)'], ['translate(0,0px)']);
      break;
  }
}

function transImg(img)
{
  console.log("Show img " + img);
  var transtime = 0.8;
  switch (img) {
    case 1:
      $("#front-img-"+img).css('transition', '0.8s ease-out');
      $("#front-img-"+img).css('transform', 'translate(0)');
    break;
    case 2:
      $("#front-img-"+(img-1)).css('transform', 'translateX(-1000px)');
      $("#front-img-"+img).css('transition', transtime +'s ease-out');
      $("#front-img-"+img).css('transform', 'translate(0, 15px)');
    break;
    case 3:
      $("#front-img-"+(img-1)).css('transform', 'translate(-1200px, 20px)');
      $("#front-img-"+img).css('transition', transtime +'s ease-out');
      $("#front-img-"+img).css('transform', 'translateX(-210px)');
    break;
    case 4:      
      $("#front-img-"+img).css('transition', transtime +'s ease-out');
      $("#front-img-"+img).css('transform', 'translate(210px)');
    break;
    case 5:
      $("#front-img-"+(img-2)).css('transform', 'translateX(-1000px)');
      $("#front-img-"+(img-1)).css('transform', 'translateX(-1000px)');
      $("#front-img-"+img).css('transition', transtime +'s ease-out');
      $("#front-img-"+img).css('transform', 'translateX(-287px) scale(0.90)');
    break;
    case 6:
      $("#front-img-"+img).css('transition', transtime +'s ease-out');
      $("#front-img-"+img).css('transform', 'translate(0) scale(0.90)');
    break;
    case 7:
      $("#front-img-"+img).css('transition', transtime +'s ease-out');
      $("#front-img-"+img).css('transform', 'translateX(287px) scale(0.90)');
      break;
    case 8:
      $("#front-img-"+(img-3)).css('transform', 'translateX(-1000px)');
      $("#front-img-"+(img-2)).css('transform', 'translateX(-1000px)');
      $("#front-img-"+(img-1)).css('transform', 'translateX(-1000px)');
      $("#front-img-"+img).css('transition', transtime+'s ease-out');
      $("#front-img-"+img).css('transform', 'translateX(-150px)');
      break;
    case 9:
      $("#front-img-"+img).css('transition', transtime+'s ease-out');
      $("#front-img-"+img).css('transform', 'translateX(150px)');
    break;
    case 10:
      $("#front-img-"+img).css('transition', transtime+'s ease-out');
      $("#front-img-"+img).css('transform', 'translateX(-150px)');
    break;
    case 11:
      $("#front-img-"+img).css('transition', transtime+'s ease-out');
      $("#front-img-"+img).css('transform', 'translateX(150px)');
    break;
    case 12:
      $("#front-img-"+(img-4)).css('transform', 'translateX(-1200px)');
      $("#front-img-"+(img-3)).css('transform', 'translateX(-1000px)');
      $("#front-img-"+(img-2)).css('transform', 'translateX(-900px)');
      $("#front-img-"+(img-1)).css('transform', 'translateX(-900px)');
      $("#front-img-"+img).css('transition', transtime+'s ease-out');
      $("#front-img-"+img).css('transform', 'translate(0 , 15px)');
    break;
    // case 13:
    //   $("#front-img-"+(img-1)).css('transform', 'translateX(-1000px)');
    //   $("#front-img-"+img).css('transition', '0.5s ease-out');
    //   $("#front-img-"+img).css('transform', 'translate(-300px)');
    // break;
    // case 14:
    //   $("#front-img-"+img).css('transition', '0.5s ease-out');
    //   $("#front-img-"+img).css('transform', 'translateX(290px)');
    // break;
    // case 15:
    //   $("#front-img-"+(img-6)).css('transform', 'translateY(-1000px)');
    //   $("#front-img-"+(img-5)).css('transform', 'translateY(-1000px)');
    //   $("#front-img-"+(img-4)).css('transform', 'translateY(-1000px)');
    //   $("#front-img-"+(img-3)).css('transform', 'translateY(1000px)');
    //   $("#front-img-"+(img-2)).css('transform', 'translateY(1000px)');
    //   $("#front-img-"+(img-1)).css('transform', 'translateY(1000px)');
    //   $("#front-img-"+img).css('transition', '0.5s ease-out');
    //   $("#front-img-"+img).css('transform', 'translate(0)');
    // break;
    case 13:
      $("#front-img-"+(img-1)).css('transform', 'translateX(-1000px)');
      $("#front-img-"+img).css('transition', transtime+'s ease-out');
      $("#front-img-"+img).css('transform', 'translate(0)');
    break;
    case 14:
      $("#front-img-"+(img-1)).css('transform', 'translateX(-1000px)');
      $("#front-img-"+img).css('transition', transtime+'s ease-out');
      $("#front-img-"+img).css('transform', 'translate(0)');
    break;
    case 15:
      $("#front-img-"+(img-1)).css('transform', 'translateY(1000px)');
      break;
  }
}

function setAnimation(ele , startProp , endProp , duration)
{
  if( duration == undefined) duration = 0.5;

  if( startProp ){
    ele.css('transition', 'transform 0.0s ease-out');
    ele.css("transform", startProp.join(" "));
  }

  setTimeout(function(){
    ele.css('transition', 'transform '+duration+'s ease-out');
    ele.css("transform", endProp.join(" "));
  },200);

}

function stop()
{
  //clearInterval(window.intervalID );
  window.callStop = true;
}

function gotoTrans(index)
{
  stop();
  trans(index);
}
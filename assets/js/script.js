var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1 && navigator.userAgent.toLowerCase().indexOf('windows') > -1;
var is_ie = navigator.userAgent.toLowerCase().indexOf('msie') > -1;
var is_dev = window.location.href.indexOf("localhost") > -1 || window.location.href.indexOf("192.168") > -1;
var selectedLocationId = '0-0';
var selectedIdType = -1;
var activeSection = -1;
var galLoading = false;
var clickEvent = 'click';

var galleryIndex = -1;
var oldGalleryIndex = -1;
var galleryOffset = 0;
var stopGalleryReload = false;

var hideReg = true;
var fullGallery = true;

var galData = [];

if (screen.width < 500) {
    var mvp = document.getElementById('myViewport');
    mvp.setAttribute('content','width=500');
}

if(is_ie || is_chrome){
    if (window.addEventListener) window.addEventListener('DOMMouseScroll', wheel, false);
    window.onmousewheel = document.onmousewheel = wheel;

    var time = 600;
    var distance = 600;

    function wheel(event) {
        if (event.wheelDelta) delta = event.wheelDelta / 120;
        else if (event.detail) delta = -event.detail / 3;

        handle();
        if (event.preventDefault) event.preventDefault();
        event.returnValue = false;
    }

    function handle() {

        $('html, body').stop().animate({
            scrollTop: $(window).scrollTop() - (distance * delta)
        }, time);
    }
}




$( document ).ready(function() {
    console.log('init...');
    var bkkLocation = [];

    if( hideReg ){
        $('#sec-register').hide();
        $('#side-tab-fixed').hide();
        $('#navbar ul li [data-target="#sec-register"]').hide();
        $('#mapPopup .map-button-area').hide();
    }

    //if( fullGallery ){
    //    $('#sec-gallery').hide();
    //    $('#sec-gallery2').show();
    //} else {
    //    $('#sec-gallery').show();
    //    $('#sec-gallery2').hide();
    //}

    function addPlaceToLi( $target , main , place , id){
        place.main = main;
        place.text =  place.main + ' - ' + place.place;
        place.locationId = id;

        var ele = '<li data-location-id="'+id+'">'+ place.text +'</li>'

        $target.append(ele);

        return place;
    }

    for (var i = 0 ; i < Object.keys(pinData[0].placeList).length ; i++ ){
        var place = pinData[0].placeList[i];
        bkkLocation.push( addPlaceToLi( $('#sub-location1 .dropdown-menu') , pinData[0].title , place , '0-' + i));
    }


    var otherLocation = [];
    for (var i = 1 ; i < pinData.length ; i++ ){
        var place = pinData[i].placeList[0];
        otherLocation.push(addPlaceToLi($('#sub-location2 .dropdown-menu') , pinData[i].title , place , i+'-0'));
    }


    // ------------- general -------------
    //scale the page
    function scaleContent(){
        var mapHeight = window.innerHeight - 400;
        if( mapHeight < 300) mapHeight = 300;
        $('#map-canvas').css('height' , mapHeight);
        $('#map-container').css('height' , mapHeight+ 100);

        return;
        if( window.innerWidth < 500){
            var scale = window.innerWidth / 500;


            $('.container-fluid').css('width' ,   '500px');
            $('.container-fluid').css('transform' , 'scale(' + scale + ')');
            $('.container-fluid').css('-webkit-transform' , 'scale(' + scale + ')');

            console.log('Scale content > ' + scale);
        }
    }
    scaleContent();


    //$('.header-block').css('height' , ( parseFloat($(window).height()) - 100) + 'px' );

    function displaySection(index){
        console.log(' section index : ' + index);
        if( index == activeSection){
            return;
        }
        var showIndex = index;
        if( index == 2 && hideReg){
            showIndex = 1;;
        }


        $('#navbar .nav [data-target]').parent('li').removeClass('active');
        //$($('#navbar .nav [data-target]').get(index)).addClass('active');
        $($('#navbar .nav [data-target]').get(showIndex)).parent('li').addClass('active')
        //$($('#navbar .nav [data-target]').get(0)).attr('data-target')

        activeSection = index;





    }
    displaySection(0);

    $('.nav-link').on(clickEvent , function(e){
        var target = $(this).attr('data-target');



        $('html,body').animate({
            scrollTop: $(target).offset().top - 100
        })
    });


    var section_offset = 100;
    var ht = $('#sec-header').offset().top - section_offset;
    var mt = $('#sec-map').offset().top - section_offset;
    var rt = $('#sec-register').offset().top - section_offset;
    var gt = $('#sec-gallery2').offset().top - section_offset;
    var end = $('body').height();
    var mc = 2;

    var secList = [];
    if ( hideReg ){
        //secList = [ht, mt, gt , end];
        secList = [ht, mt , rt , gt ,end];
    } else {
        secList = [ht, mt , rt , gt ,end];
    }



    $(window).scroll(function (event) {
        var scroll = $(window).scrollTop();

        //console.log(scroll);
        // section
        var sectionIndex = 0;
        for( var i = 0 ; i < secList.length ; i++ ){
            if( scroll < secList[i] - 120) {
                sectionIndex = i;
                break;
            }
        }

        displaySection(sectionIndex - 1);




        // pallalax
        // 0 - 300
        if(  scroll < 500){

            var p = (scroll) / 500 * 100;
            p = p > 0 ? p : 0;
            p = p < 100 ? p : 100;
            p *= mc;
            //$('.header-bg').css('background-position' , 'center '+ p + '%');
            //console.log(p);

            $('#sec-header .pallalax-bg').css('transform' , 'scale(1.2) translate(0 , '+ p +'px)');
        }


        if(scroll > mt & scroll < rt){
            var p = (scroll - mt) / (rt - mt) * 100;
            p = p > 0 ? p : 0;
            p = p < 100 ? p : 100;
            p *= mc;

            $('#sec-map .pallalax-bg').css('transform' , 'scale(1.2) translate(0 , '+ p +'px)');
        }

        if(scroll > rt & scroll < gt){
            var p = (scroll - rt) / (gt - rt) * 100;
            p = p > 0 ? p : 0;
            p = p < 100 ? p : 100;
            p *= mc;

            $('#sec-register .pallalax-bg').css('transform' , 'scale(1.2) translate(0 , '+ p +'px)');
        }


        //$('#side-tab-fixed').css('transform' , 'translate(0,'+(scroll)+'px)');
        if( scroll > rt && scroll < gt){
            $('#side-tab-fixed').css('opacity' , 0);
        } else {
            $('#side-tab-fixed').css('opacity' , 1);

            $('#side-tab-fixed').stop();
            $('#side-tab-fixed').animate({
                top: (200 + scroll) + "px"
            } , 500 );
        }


    });


    // ------------- video -------------
    $('#sec-video .video-list li').on(clickEvent, function(e){
        $('#sec-video .video-list li').each(function(e){
            $(this).removeClass('active');
        });
        $(this).addClass('active');

        console.log('play vidoe id : '+ $(this).attr('data-video-src'));
        var url = "http://www.youtube.com/embed/" + $(this).attr('data-video-src') + "?autoplay=1";
        $('#youtubeVideoFrame').attr('src' ,url );
        $('#videoCaption').html($(this).attr('data-video-caption'));

    });

    // set thumb



    // ------------- map -------------

    //$('.map-pin').popover(options);

//    $('[data-toggle=popover]').on('show.bs.popover', function () {
//        $('.popover').css('top',parseInt($(this).css('top')) + 22 + 'px !important')
//    })

    var mapOptions = {
        center: new google.maps.LatLng(-34.397, 150.644),
        zoom: 14
    };
    window.map = new google.maps.Map(document.getElementById("map-canvas"),
        mapOptions);


    $('#sec-map .pallalax-bg, #sec-video , #sec-register').on(clickEvent, function() {
        //Hide the menus if visible
        $('#mapPopover').hide();
    });

    var currentMark = null;
    function showMapById(id){

        var res = id.split('-');
        var placeData = pinData[res[0]].placeList[res[1]];

        var isBangkok = res[0] == 0;
        if( isBangkok ){
            // set first drop down

            var subIndex = parseInt($(this).attr('data-place-id'));

            $('#sub-location1').show();
            $('#sub-location2').hide();

            $('#main-location p').html('7 จุดในกรุงเทพฯ ');
            $('#sub-location1 p').html(placeData.text);
        } else {
            $('#sub-location1').hide();
            $('#sub-location2').show();

            $('#main-location p').html('14 จุดทั่วประเทศ');
            $('#sub-location2 p').html(placeData.text);
        }

        $('#map-container .map-footer p').html( "<span>" + placeData.place + "</span></br>" + placeData.act + " | " + placeData.time);

//        var $footer = $('#map-container .map-footer');
//        var maxWidth = 580;
//        console.log('Map footer scale 2 : ' + $footer.width() + " > " + document.oriFooterWidth + " > " + maxWidth);
//        if( typeof document.oriFooterWidth == 'undefined' && $footer.width() != 100 ){
//            document.oriFooterWidth = window.innerWidth - 80;
//        }
//
//        document.oriFooterWidth = window.innerWidth - 80;
//        if(document.oriFooterWidth <= maxWidth){
//            var scale = document.oriFooterWidth / maxWidth;
//
//            $footer.css('width', maxWidth);
//            $footer.css('transform' , 'scale(' + scale + ')');
//            $footer.css('-webkit-transform' , 'scale(' + scale + ')');
//            $footer.css('transform-origin' , '0 100%');
//            $footer.css('-webkit-transform-origin' , '0 100%');
//
//            //console.log('Map footer scale : ' + scale + " > " + document.oriFooterWidth + " > " + maxWidth);
//
//        } else {
//            var scale = 1;
//            $footer.css('width', "100%");
//            $footer.css('transform' , 'scale(' + scale + ')');
//            $footer.css('-webkit-transform' , 'scale(' + scale + ')');
//
//
//            //console.log('Map footer scale : ' + scale);
//
//        }
//
//        console.log('Map footer scale : ' + scale + " > " + document.oriFooterWidth + " > " + maxWidth);



        var pos = new google.maps.LatLng(placeData.lat, placeData.long);
        // clear old marker
        if( currentMark ){
            currentMark.setMap(null);
        }


        var marker = new google.maps.Marker({
            position: pos,
            map: window.map,
            title: 'Hello World!'
        });

        currentMark = marker;

        window.map.setCenter(pos);

        setTimeout(function(){
            google.maps.event.trigger(window.map, 'resize');
            window.map.setCenter(pos);
        } , 250);


        selectedLocationId = id;

        console.log('Show map id:' + id);
        console.log('Show map text:' + placeData.text);
    }

    $("#mapPopup .dropdown-menu li").click(function(){
        var selText = $(this).text();
        $(this).parents('.dropdown').find('p').html(selText);

        // update map
        showMapById($(this).attr('data-location-id'));
    });

    $('#mapImg').on('mouseout', function(e){
        $('#mapPopover').hide();
    });


    $('.map-pin').on('mouseover touchend', function(e){
        var pinId = $(this).attr('pin-id');
        var data = pinData[pinId];



        // set up pop up
        $mp = $('#mapPopover');
        $mp.show();
        var ptop =  parseFloat($(this).css('top')) - ($mp.height() * 0);
        var pleft = parseFloat($(this).css('left')) - ($mp.width() * 0.3);
        pleft = pleft > 200 ? 200 : pleft;

        $mp.css('top',ptop);
        $mp.css('left', pleft );


        var content = '<ul>';
        for( var i = 0 ; i < Object.keys(data.placeList).length ; i ++){
            var active = i == 0 ? ' class="active" ' : ' ';
            content += '<li '+ active +' data-pin-id="'+pinId+'" data-place-id="'+i+'" >'
            content += data.placeList[i].place;
            content += '</li>';
        }
        content += '</ul>';

        $mp.children('.popover-content').html(content);
        $mp.children('.popover-title').html(data.title);


        var hilightItem = null;
        $('#mapPopover .popover-content li').on('mouseover touchstart' , function(){
            $('#mapPopover .popover-content li').removeClass('active');
            $(this).addClass('active');

            hilightItem = $(this).html();
        });



        $('#mapPopover .popover-content li').on('click touchend', function(e){
            if( hilightItem != $(this).html() ) return;

            $mp.hide();

            var placeData = data.placeList[ $(this).attr('data-place-id')];
            console.log(placeData);
            $('html,body').animate({
                scrollTop: $('#sec-map').offset().top - 100
            }, 250,  function(){


                $('#mapPopup').modal('show');
                showMapById(placeData.locationId);
            });


            e.stopPropagation();
        });


        e.stopPropagation();
    });

    $('.map-pin').on('mouseover', function(e){
        $(this).attr('src', 'assets/img/pin_red.png');
    });

    $('.map-pin').on('mouseout', function(e){
        $(this).attr('src', 'assets/img/pin_dark.png');
    });



    var srcWidth = 524;
    var srcHeight = 752;


    function refreshPin(){

        //console.log("Resize to : " + );
        var baseWidth = 524;
        var baseHeight = 752;
        var width = $('#mapImg').width();
        var height = $('#mapImg').height();

        var scaleX = width / baseWidth;
        var scaleY = height / baseHeight;

        $( ".map-pin" ).each(function( index ) {
            //console.log( index + ": " + pinData[index].title);
            var data = pinData[index];
            $(this).css('left', (data.x * scaleX)  +'px')
            $(this).css('top', (data.y * scaleY) + 'px');


        });

        console.log("Scale map : " + scaleX + " > " + scaleY);
    }

    //refreshPin();
    setTimeout(refreshPin , 250);

    $(window).on('resize', function(){
        refreshPin();
        scaleContent();
    });


    function verifyData(){

        if( is_dev ) return true;

        var name = $('#inputName').val();
        var id = $('#inputId').val();
        var tel = $('#inputTel').val();
        var email = $('#inputEmail').val();

        var error = 0;

        if( !name ){
            $('#inputName').addClass('error');
            error++;
        } else {
            $('#inputName').removeClass('error');
        }


        if( selectedIdType  == -1){
            $('#dropdownMenu1').addClass('error');
            error++;
        } else {
            $('#dropdownMenu1').removeClass('error');
        }

        if( selectedIdType == 0 || selectedIdType == 1){

            if( id.length  != 13){
                $('#inputId').addClass('error');
                error++;
            } else {
                $('#inputId').removeClass('error');
            }
        } else {
            if( id.length  < 5 || id.length > 8){
                $('#inputId').addClass('error');
                error++;
            } else {
                $('#inputId').removeClass('error');
            }
        }


        if( tel.length  < 7 || tel.length > 11){
            $('#inputTel').addClass('error');
            error++;
        } else {
            $('#inputTel').removeClass('error');
        }


        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(! re.test(email)){
            // email error;
            $('#inputEmail').addClass('error');
            error++;
        } else {
            $('#inputEmail').removeClass('error');
        }

        if( error > 0 ){
            return false;
        } else {
            return true;
        }

    }

    $('#mapRegisterBtn').on(clickEvent , function(e){
        $('#mapPopup').modal('hide');

    });

    $('#registerBtn').on(clickEvent , function(e){
        //console.log('');
        //$('#mapPopup').show();
        //$('#mapPopup').css('opacity' , 1);
        //alert('tap!');


        var isPass = verifyData();

        if( isPass ){

            $('#mapRegisterBtn').hide();
            $('#mapConfirmBtn').show();

            console.log($('#sec-map').offset().top - 100);
            $('html,body').animate({
                scrollTop: $('#sec-map').offset().top - 100
            }, 350,  function(){


                $('#mapPopup').modal('show');
                showMapById(selectedLocationId);
            });

        }
    });

    $('#mapConfirmBtn').on(clickEvent , function(e){
        console.log('');
        $('#mapPopup').modal('hide');


        // send data & save


        // show thank message
        $('#register').hide();
        $('#register-complete').show();



        var data = {
            'name': $('#inputName').val(),
            'tel':$('#inputTel').val(),
            'locationId': selectedLocationId,
            'email': $('#inputEmail').val(),
            'code': $('#inputId').val(),
            'type': selectedIdType
        };

        $.post( "data.php", data , function( rtn ) {
            console.log(rtn);
        });
    });



    // ------------- register -------------
    $('.numberonly').keyup(function() {
        $(this).val($(this).val().match(/\d*\.?\d+/));
    });


    $("#register-id-type .dropdown-menu li").click(function(){
        var selText = $(this).text();
        $('#dropdownMenu1').html(selText);

        var id = $(this).attr('data-id');

        var text = {0:'เลขที่บัตรประชาชน' , 1:'เลขที่บัตรประชาชน' , 2:'รหัสตัวแทน'};
        $('#register-id-type-label').html(text[id]);

        if(id == 0 || id == 1){
            $('#inputId').attr('maxlength' , 13);
        } else {
            $('#inputId').attr('maxlength' , 8);
        }
        $('#inputId').val('');

        selectedIdType = id;
    });



    // ------------- gallery -------------


    //$('.thumbnail.old').click(function(e){
    //    $img = $(this).find('img');
    //    var src = $img.attr('src');
    //    $("#galleryPopup .display").attr('src' , src);
    //    console.log(src);
    //});

    //$(document).on("click", ".thumbnail.old", function() {
    //    $img = $(this).find('img');
    //    var src = $img.attr('src');
    //    $("#galleryPopup .display").attr('src' , src);
    //    console.log(src);
    //});

    function setGalData( index ){
        console.log("set gal data = " + index);
        var data = galData[index];
        var id = data.id;


        console.log(data);
        if( typeof data == 'string'){
            $("#galleryPopup .display").attr('src' , data);
        } else {

            $('#galleryPopupFull .detail>.header>.user').css('background-image', "url("+data.user_pic+")");
            $('#galleryPopupFull .detail>.header>span').html(data.user_fullname);
            $('#galleryPopupFull .detail>.content').html(data.caption);
            //$('#galleryPopupFull .detail>.footer>.footer-like').html("♥ " + data.likes);
            $('#galleryPopupFull .detail>.footer>.footer-like>a>span').html(data.likes);
            $('#galleryPopupFull .detail>.footer>.footer-like>a').attr('href', data.source_link);

            var path = data.external_id  + '.jpg';
            var src = 'images/img/' + path;
            $("#galleryPopupFull .display").attr('src' , src);
        }

    }

    function setOldGalData( index ){
        console.log("set old gal data = " + index);
        var src = "assets/photo/" + index + ".JPG";
        $("#galleryPopup .display").attr('src' , src);
    }


    //$('#sec-gallery2 .thumbnail.new').click(function(e){
    //    $img = $(this).find('img');
    //    var src = $img.attr('src');
    //
    //
    //    $("#galleryPopupFull .display").attr('src' , src);
    //
    //
    //    // apply data
    //
    //    setGalData($(this).attr('data-photo-id'));
    //});

    $(document).on("click touchend", "[data-old-photo-id]", function() {
        $('#mapPopover').hide();

        var index = $(this).attr('data-old-photo-id');

        // apply data
        setOldGalData(index);
        oldGalleryIndex = parseInt(index);
    });

    $(document).on("click touchend", "[data-photo-id]", function() {
        $('#mapPopover').hide();

        //$img = $(this).find('img');
        //var src = $img.attr('src');


        var id = $(this).attr('data-photo-id');
        var index = $(this).attr('data-photo-index');

//        $("#galleryPopup .display").src="";

        // apply data
        setGalData(index);
        galleryIndex = parseInt(index);
    });

    function addGallery(data){
        for( var i = 0; i < data.length ; i++){
            var item = data[i];


            var t = '';
            var path = item.external_id  + '.jpg';
            t += '<div class="col-xs-6 col-md-3">';
            t += '<div class="thumbnail new" data-toggle="modal" data-target="#galleryPopupFull" data-photo-id="'+item.id+'" data-photo-index="'+galData.length+'">';
            //t += '  <img src="data/img/'+path+'" alt="...">';
            t += ' <div class="thumb-image" style="background-image:url(images/img/'+path+')" ></div>';
            t += '</div></div>';
            $('#sec-gallery2 .row').append(t)

            //galData[item.id] = item;
            galData.push(item);
        }


    }

    function loadGallery(offset){
        var limit = 20;

        if( galLoading ) return;
        galLoading = true;
        $.post('data.php' , {'offset':offset , 'limit':limit} , function(response){
            galLoading = false;
            console.log('Search data');
            console.log(response);
            $('#gallery-loading').animate({height:0});

            var re = JSON.parse(response);

            addGallery(re);

            galleryOffset += limit;

            if( re.length < 20 ){

                stopGalleryReload = true;
//                for( var i = 1; i < 17 ; i++){
//
//                    var path = 'assets/photo/' + i + '.JPG';
//                    t = '';
//                    t += '<div class="col-xs-6 col-md-3">';
//                    t += '<div class="thumbnail old" data-toggle="modal" data-target="#galleryPopup" data-old-photo-id="'+i+'">';
//                    //t += '  <img src="'+path+'" alt="...">';
//                    t += ' <div class="thumb-image" style="background-image:url('+path+')" ></div>';
//                    t += '</div></div>';
//                    $('#sec-gallery2 .row').append(t)
//                }
            }





        });
    }

    if( fullGallery ){
        loadGallery(0);
    } else {
        loadGallery(1000000);
    }


    $(window).scroll(function(){
        if(stopGalleryReload) return;


        if($(window).scrollTop() + $(window).height() == $(document).height()) {
            console.log("bottom!");

            if(galleryOffset == 0) return;
            $('#gallery-loading').animate({height:30});
            loadGallery(galleryOffset);
        }
    });


    function getGalIndex(id){

    }

    function getGalId(index){
        //var i = 0;
        //for( var k in galData){
        //    if( i == index){
        //        return k;
        //    }
        //    i++;
        //}
        for( var i = 0 ; i < galData.length ; i ++){
            //if( galData[i].id
        }
    }

    $('.gal-arrow.left.new').click(function(){

        if( galleryIndex > 0){
            setGalData(galleryIndex - 1);
            galleryIndex -= 1;
        }

    });

    $('.gal-arrow.right.new').click(function(){

        if( galleryIndex < galData.length -1){
            setGalData(galleryIndex + 1);
            galleryIndex += 1;
        } else {
            // show old gallery
            oldGalleryIndex = 1;
            setOldGalData(1);
            $('#galleryPopup').modal('show');
            $('#galleryPopupFull').modal('hide');
        }
    });

    $('.gal-arrow.left.old').click(function(){

        if( oldGalleryIndex > 1){
            setOldGalData(oldGalleryIndex - 1);
            oldGalleryIndex -= 1;
        } else {
            // show new gallery
            if( fullGallery ){
                galleryIndex = galData.length -1;
                setGalData(galData.length-1)
                $('#galleryPopup').modal('hide');
                $('#galleryPopupFull').modal('show');
            }
        }

    });

    $('.gal-arrow.right.old').click(function(){

        if( oldGalleryIndex < 16){
            setOldGalData(oldGalleryIndex + 1);
            oldGalleryIndex += 1;
        }
    });


});